//Arrow Functions
//ES 6 ECMAScript

//Function Declaration
function sum(a,b){
	return a+b	
}

//Expression


//Step 1
// let sum2

//Step 2 
// let sum2 = () =>

//Step3
//let sum2 = (a,b) =>{
//	return a+b
//}

//Arrow function makes your code shorter and cleaner. 
//No need for curly braces and return keyword.
//let sum2 = (a,b) =>a+b

//function isPositive(a){
//	return a >= 0
//}
//let isPositive2 = (a) =>{
//	return a >= 0
//}

//let isPositive2 = a => a >= 0

//function someNumber(){
//	return Math.random()
//}

//let someNumber2 = () => {
//	return Math.random()
//}
//convert to arrow statement
//let someNumber2 = () => Math.random()

function hello(){
	return "Hello World!";
}
//Convert to Arrow Function
//let hello = () => "Hello World!"

function square(a){
	return a * a;
}
//Convert to Arrow Function
//let square = (a) => a * a;

function giveAnswer(num1, num2){
	let total = num1 + num2
	return total
}
//Convert To Arrow Function
let giveAnswer2 = (num1, num2) => {
	let total = num1 + num2
	return total
}

//Object Destructuring

const address = {
	street: 'Times St.',
	city: 'New York',
	country: 'USA',
}

// const street = address.street;
// const city = address.city;
// const country = address.country;

const {city, street, country} = address

//Template Literals
let name = 'Jino'
let job = 'Instructor'

console.log("Hi! My name is " + name + " I'm your " + job)

console.log(`Hi! My name is ${name}`)

//Array Map Method
const colors = ['red','green','blue']

// const items = colors.map(function(color){
// 	return"<li>" + color + "</li>";	
// })

 // <li>red</li>
 // <li>green</li>
 // <li>blue</li>

const items = colors.map(color =>`<li> ${color} </li>`)

